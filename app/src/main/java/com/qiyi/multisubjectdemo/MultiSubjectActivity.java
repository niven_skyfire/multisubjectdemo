package com.qiyi.multisubjectdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.qiyi.multisubjectdemo.data.Content;
import com.qiyi.multisubjectdemo.data.MultiSubjectModel;
import com.qiyi.multisubjectdemo.data.Subject;
import com.qiyi.multisubjectdemo.data.VIPAdsData;
import com.qiyi.multisubjectdemo.data.VideoData;
import com.qiyi.multisubjectdemo.mvp.MultiSubjectContract;
import com.qiyi.multisubjectdemo.mvp.MultiSubjectPresenter;

import java.util.List;

public class MultiSubjectActivity extends AppCompatActivity implements MultiSubjectContract.MvpView {

    private MultiSubjectContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MultiSubjectPresenter(this);
        presenter.subscribe();
    }

    @Override
    public void setBackground(String imgUrl) {

    }

    @Override
    public void setNestList(MultiSubjectModel multiSubjectModel) {

    }

    @Override
    public void setSubjectList(List<Subject> subjectList) {

    }

    @Override
    public void setContentList(List<Content> contentList) {

    }

    @Override
    public void showVIPAds(VIPAdsData vipAdsData) {

    }

    @Override
    public void switchVideo(VideoData videoData) {

    }
}
