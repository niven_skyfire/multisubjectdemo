package com.qiyi.multisubjectdemo.server;

import android.util.SparseArray;

import com.qiyi.multisubjectdemo.data.BaseListData;
import com.qiyi.multisubjectdemo.data.Category;
import com.qiyi.multisubjectdemo.data.Content;
import com.qiyi.multisubjectdemo.data.MultiSubjectModel;
import com.qiyi.multisubjectdemo.data.Subject;
import com.qiyi.multisubjectdemo.data.VIPAdsData;
import com.qiyi.multisubjectdemo.data.VideoData;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by heweibing on 2018/10/11
 * heweibing@qiyi.com
 */
public class MockServer {
    static String[] categoryArr = new String[]{
            "新闻",
            "电影",
            "电视剧",
            "生活",
            "娱乐",
    };

    static String[] subjectArr = new String[]{
            "碟中谍",
            "印度",
            "007",
            "魔戒",
            "东方",
            "喜剧",
    };

    static String[] contentArr = new String[]{
            "《海王》5分钟超长中文预告片尽展视效奇观 动作场面登峰造极",
            "韩寒新片《飞驰人生》首支预告片 逗我呢！沈腾开赛车和飞机飙车",
            "沈腾首次演爸爸被儿子狠坑，韩寒执导新片《飞驰人生》先导预告片 ",
            "伏狐记：运镖途中遇僵尸，假道士误打误撞揭穿阴谋",
            "袁彩云竟是绑匪帮凶！ 叶德娴竟然猜中",
            "张家辉玩命真打，却拍出了这种尬电影！",
            "黑白森林（片段）社会我春哥，人狠话不多! ",
            "奶奶嘲笑爷爷 换来爷爷的“一纸休书” ",
            "阿宝用自己把天煞带回灵界 画面太美了",
    };

    static String[] imgArr = new String[]{
            "https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3866496738,3596841422&fm=26&gp=0.jpg",
            "https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3878624857,2668019451&fm=26&gp=0.jpg",
            "https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1817716169,2875518587&fm=26&gp=0.jpg",
            "https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=184364899,3634765816&fm=26&gp=0.jpg",
            "https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3502623055,3186529747&fm=26&gp=0.jpg",
            "https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3603467389,3194474154&fm=26&gp=0.jpg",
            "https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3340738199,304333860&fm=26&gp=0.jpg",
            "https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2192283649,1157513088&fm=26&gp=0.jpg",
            "https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=936023209,2815732888&fm=26&gp=0.jpg",
    };

    static String[] videoArr = new String[]{
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/SubaruOutbackOnStreetAndDirt.mp4",
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4",
    };

    static String[] adsUrl = new String[]{
            "https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2517612170,2234981949&fm=200&gp=0.jpg",
            "https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=1565245327,1507552098&fm=200&gp=0.jpg",
            "https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2671814059,879259453&fm=200&gp=0.jpg",
            "https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2596177124,1742410845&fm=26&gp=0.jpg",
    };

    public static MultiSubjectModel getMultiSubjectData() {
        MultiSubjectModel multiSubjectModel = new MultiSubjectModel();
        SparseArray<List<BaseListData>> listSparseArray = new SparseArray<>();
        boolean is3Layer = new Random().nextBoolean();

        List<BaseListData> categoryList = new ArrayList<>();
        if (is3Layer) {
            //add category
            for (String str : categoryArr) {
                Category category = new Category();
                category.title = str;
                List<Subject> subjectList = new ArrayList<>();
                int resId = 0;
                for (String tmp : subjectArr) {
                    Subject subject = new Subject();
                    subject.title = tmp;
                    subject.resId = resId + "";
                    subjectList.add(subject);
                    resId++;
                }
                category.subjectList = subjectList;
                categoryList.add(category);
            }
            listSparseArray.append(0, categoryList);

            //add content
            List<BaseListData> contentList = new ArrayList<>();
            for (int i = 0; i < 30; i++) {
                String title = "resId:0" + " " + contentArr[i % (categoryArr.length - 1)];
                Content content = new Content();
                content.title = title;
                content.isAlbum = new Random().nextBoolean();
                content.imgUrl = imgArr[i % (imgArr.length - 1)];
                VideoData videoData = new VideoData();
                videoData.title = title;
                videoData.playUrl = videoArr[i % (videoArr.length - 1)];
                contentList.add(content);
            }
            listSparseArray.append(1, contentList);
        } else {
            //add subject
            List<BaseListData> subjectList = new ArrayList<>();
            int resId = 0;
            for (String str : subjectArr) {
                Subject subject = new Subject();
                subject.title = str;
                subject.resId = resId + "";
                resId++;
                subjectList.add(subject);
            }
            listSparseArray.append(0, subjectList);

            //add content
            List<BaseListData> contentList = new ArrayList<>();
            for (int i = 0; i < 30; i++) {
                String title = "resId:0" + " " + contentArr[i % (categoryArr.length - 1)];
                Content content = new Content();
                content.title = title;
                content.isAlbum = new Random().nextBoolean();
                content.imgUrl = imgArr[i % (imgArr.length - 1)];
                VideoData videoData = new VideoData();
                videoData.title = title;
                videoData.playUrl = videoArr[i % (videoArr.length - 1)];
                contentList.add(content);
            }
            listSparseArray.append(1, contentList);
        }

        multiSubjectModel.listData = listSparseArray;

        multiSubjectModel.backgroundUrl = "http://www.deskcar.com/desktop/else/201272223323/10.jpg";

        return multiSubjectModel;
    }

    public static List<Content> getContentList(String resId) {
        List<Content> contentList = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            String title = "resId:" + resId + " " + contentArr[i % (categoryArr.length - 1)];
            Content content = new Content();
            content.title = title;
            content.isAlbum = new Random().nextBoolean();
            content.imgUrl = imgArr[i % (imgArr.length - 1)];
            VideoData videoData = new VideoData();
            videoData.title = title;
            videoData.playUrl = videoArr[i % (videoArr.length - 1)];
            contentList.add(content);
        }
        return contentList;
    }

    public static VIPAdsData getVipAdsData() {
        Random random = new Random();
        VIPAdsData vipAdsData = new VIPAdsData();
        vipAdsData.url = adsUrl[random.nextInt(adsUrl.length - 1)];
        return vipAdsData;
    }
}
