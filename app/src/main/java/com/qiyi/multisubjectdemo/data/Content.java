package com.qiyi.multisubjectdemo.data;

/**
 * Created by heweibing on 2018/10/11
 * heweibing@qiyi.com
 * 三级目录
 */
public class Content extends BaseListData {
    public String title;
    public String imgUrl;
    public VideoData videoData;
    //是否播单数据
    public boolean isAlbum;

}
