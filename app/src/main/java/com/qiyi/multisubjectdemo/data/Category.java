package com.qiyi.multisubjectdemo.data;

import java.util.List;

/**
 * Created by heweibing on 2018/10/11
 * heweibing@qiyi.com
 * 一级目录
 */
public class Category extends BaseListData {
    public String title;
    public List<Subject> subjectList;
}
