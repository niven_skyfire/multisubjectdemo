package com.qiyi.multisubjectdemo.data;

import android.util.SparseArray;

import java.util.List;

/**
 * Created by heweibing on 2018/10/11
 * heweibing@qiyi.com
 */
public class MultiSubjectModel {

    //size恒等于2,key==0处的数据类型若为Category则为三层,否则为两层
    public SparseArray<List<BaseListData>> listData;

    //背景图
    public String backgroundUrl;
}
