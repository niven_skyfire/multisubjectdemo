package com.qiyi.multisubjectdemo.mvp;

/**
 * Created by heweibing on 2018/10/11
 * heweibing@qiyi.com
 */
public interface BasePresenter {
    void subscribe();

    void unsubscribe();
}
