package com.qiyi.multisubjectdemo.mvp;

import com.qiyi.multisubjectdemo.data.Content;
import com.qiyi.multisubjectdemo.data.MultiSubjectModel;
import com.qiyi.multisubjectdemo.data.Subject;
import com.qiyi.multisubjectdemo.data.VIPAdsData;
import com.qiyi.multisubjectdemo.data.VideoData;

import java.util.List;

/**
 * Created by heweibing on 2018/10/11
 * heweibing@qiyi.com
 */
public interface MultiSubjectContract {

    interface MvpView extends BaseMvpView<Presenter> {

        /**
         * 设置背景图
         * @param imgUrl 背景图地址
         */
        void setBackground(String imgUrl);

        /**
         * 设置列表数据
         * @param multiSubjectModel 列表数据
         */
        void setNestList(MultiSubjectModel multiSubjectModel);

        /**
         * 设置二级目录数据
         * @param subjectList 二级目录数据
         */
        void setSubjectList(List<Subject> subjectList);

        /**
         * 设置三级目录数据
         * @param contentList 三级目录数据
         */
        void setContentList(List<Content> contentList);

        /**
         * 显示vip广告
         * @param vipAdsData vip广告数据
         */
        void showVIPAds(VIPAdsData vipAdsData);

        /**
         * 切换小窗视频以及设置视频标题
         * @param videoData 视频数据
         */
        void switchVideo(VideoData videoData);
    }

    interface Presenter extends BasePresenter {

        /**
         * 切换一级目录
         * @param categoryPosition 一级列表获焦位置
         */
        void switchCategory(int categoryPosition);

        /**
         * 切换二级目录
         * @param subjectPosition 二级列表获焦位置
         */
        void switchSubject(int subjectPosition);
    }
}
