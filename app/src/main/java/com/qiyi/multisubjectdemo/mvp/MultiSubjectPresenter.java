package com.qiyi.multisubjectdemo.mvp;

import com.qiyi.multisubjectdemo.data.BaseListData;
import com.qiyi.multisubjectdemo.data.Category;
import com.qiyi.multisubjectdemo.data.Content;
import com.qiyi.multisubjectdemo.data.MultiSubjectModel;
import com.qiyi.multisubjectdemo.data.Subject;
import com.qiyi.multisubjectdemo.data.VIPAdsData;
import com.qiyi.multisubjectdemo.server.MockServer;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by heweibing on 2018/10/12
 * heweibing@qiyi.com
 */
public class MultiSubjectPresenter implements MultiSubjectContract.Presenter {
    private MultiSubjectContract.MvpView mvpView;

    private MultiSubjectModel multiSubjectModel;
    private List<BaseListData> categoryList;
    private List<Subject> currentSubjectList;

    public MultiSubjectPresenter(MultiSubjectContract.MvpView mvpView) {
        this.mvpView = mvpView;
    }

    @Override
    public void subscribe() {
        fetchPageData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MultiSubjectModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(MultiSubjectModel multiSubjectModel) {
                        MultiSubjectPresenter.this.multiSubjectModel = multiSubjectModel;

                        //更新数据列表及背景图
                        mvpView.setBackground(multiSubjectModel.backgroundUrl);
                        mvpView.setNestList(multiSubjectModel);

                        if (multiSubjectModel.listData.get(0).get(0) instanceof Category) {
                            categoryList = multiSubjectModel.listData.get(0);
                            Category category = (Category) categoryList.get(0);
                            currentSubjectList = category.subjectList;
                        } else if (multiSubjectModel.listData.get(0).get(0) instanceof Subject) {
                            currentSubjectList = new ArrayList<>();
                            for (BaseListData baseListData : multiSubjectModel.listData.get(0)) {
                                currentSubjectList.add((Subject) baseListData);
                            }
                        }

                        //播放第一条视频
                        List<BaseListData> contentList = multiSubjectModel.listData.get(1);
                        mvpView.switchVideo(((Content) contentList.get(0)).videoData);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

        fetchVIPAdsData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<VIPAdsData>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(VIPAdsData vipAdsData) {
                        mvpView.showVIPAds(vipAdsData);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void switchCategory(int categoryPosition) {
        if (categoryList != null && categoryList.size() > 0) {
            fetchSubjectList(categoryPosition)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<List<Subject>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(List<Subject> subjects) {
                            mvpView.setSubjectList(subjects);
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
    }

    @Override
    public void switchSubject(int subjectPosition) {
        fetchContentList(subjectPosition)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Content>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Content> contents) {
                        mvpView.setContentList(contents);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * 获取二级列表数据
     *
     * @param categoryPosition 一级列表获焦位置
     */
    private Observable<List<Subject>> fetchSubjectList(final int categoryPosition) {
        return Observable.create(new ObservableOnSubscribe<List<Subject>>() {
            @Override
            public void subscribe(ObservableEmitter<List<Subject>> emitter) throws Exception {
                List<BaseListData> categoryList = multiSubjectModel.listData.get(0);
                List<Subject> subjectList = ((Category) categoryList.get(categoryPosition)).subjectList;
                if (subjectList == null || subjectList.size() == 0) {
                    emitter.onError(new Exception("empty list error"));
                } else {
                    emitter.onNext(subjectList);
                    emitter.onComplete();
                }
            }
        });
    }

    /**
     * 获取三级列表数据
     *
     * @param subjectPosition 二级列表获焦位置
     */
    private Observable<List<Content>> fetchContentList(final int subjectPosition) {
        return Observable.create(new ObservableOnSubscribe<List<Content>>() {
            @Override
            public void subscribe(ObservableEmitter<List<Content>> emitter) throws Exception {
                String resId = currentSubjectList.get(subjectPosition).resId;
                List<Content> contentList = MockServer.getContentList(resId);
                if (contentList == null || contentList.size() == 0) {
                    emitter.onError(new Exception("empty list error"));
                } else {
                    emitter.onNext(contentList);
                }
            }
        });
    }

    /**
     * 获取vip广告数据
     */
    private Observable<VIPAdsData> fetchVIPAdsData() {
        return Observable.create(new ObservableOnSubscribe<VIPAdsData>() {
            @Override
            public void subscribe(ObservableEmitter<VIPAdsData> emitter) throws Exception {
                VIPAdsData vipAdsData = MockServer.getVipAdsData();
                if (vipAdsData != null) {
                    emitter.onNext(vipAdsData);
                    emitter.onComplete();
                } else {
                    emitter.onError(new Exception("empty vipAdsData error!"));
                }
            }
        });
    }

    private Observable<MultiSubjectModel> fetchPageData() {
        return Observable.create(new ObservableOnSubscribe<MultiSubjectModel>() {
            @Override
            public void subscribe(ObservableEmitter<MultiSubjectModel> emitter) throws Exception {
                MultiSubjectModel multiSubjectModel = MockServer.getMultiSubjectData();
                if (multiSubjectModel != null) {
                    emitter.onNext(multiSubjectModel);
                    emitter.onComplete();
                } else {
                    emitter.onError(new Exception("empty multiSubjectModel error"));
                }
            }
        });
    }

    @Override
    public void unsubscribe() {
        multiSubjectModel = null;
        categoryList = null;
        currentSubjectList = null;
    }
}
